package com.zuitt.capstone.controllers;

import com.zuitt.capstone.exceptions.UserException;
import com.zuitt.capstone.models.User;
import com.zuitt.capstone.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@CrossOrigin
public class UserController {

    @Autowired
    UserService userService;

    @RequestMapping(value="/users", method = RequestMethod.POST)
    public ResponseEntity<Object> createUser(@RequestBody User user) {
        userService.createUser(user);
        return new ResponseEntity<>("User created successfully", HttpStatus.CREATED);
    }

    @RequestMapping(value="/users", method = RequestMethod.GET)
    public ResponseEntity<Object> getUsers() {
        return new ResponseEntity<>(userService.getUsers(), HttpStatus.OK);
    }

    @RequestMapping(value = "/users/register", method = RequestMethod.POST)
    public ResponseEntity<Object> register (@RequestBody Map<String, String> body) throws UserException {
        String username = body.get("username");
        if (!userService.findByUsername(username).isEmpty()) {
            throw new UserException("Username already exists.");
        }
        else {
            // This retrieves the value associated with the "password" key from the request body "Map" and assigns it to a String variable called "password".
            String password = body.get("password");

            //This encrypts the password using the BCryptPasswordEncode, and store it to the "encodedPassword" variable.
            String encodedPassword = new BCryptPasswordEncoder().encode(password);

            // Instantiates the User model to create a new user
            User newUser = new User(username, encodedPassword);

            // saves in the "newUser" in the database.
            userService.createUser(newUser);

            // Sends a "User registered successfully" message as the response body and an HTTP status code of 201.
            return new ResponseEntity<>("User registered successfully", HttpStatus.CREATED);
        }

    }
}
